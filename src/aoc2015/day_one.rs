use crate::utils::utils;

pub fn part_one() -> i32 {
    let mut result = 0;

    let lines = utils::lines_from_file("./src/aoc2015/data/day_one.data");
    for line in lines {
        let chars = utils::characters_from_line(line);
        for c in chars { 
            match c {
                '(' => { result += 1 }
                ')' => { result -= 1 }
                _   => { }
            }
        }
    }

    return result;
}

pub fn part_two() -> i32 {
    let mut result = 0;
    let mut position = 0;

    let lines = utils::lines_from_file("./src/aoc2015/data/day_one.data");
    for line in lines {
        let chars = utils::characters_from_line(line);
        for c in chars { 
            match c {
                '(' => { 
                    result += 1;
                    position += 1;
                }
                ')' => { 
                    result -= 1;
                    position += 1;
                    if result < 0 {
                        return position;
                    }
                }
                _   => { }
            }
        }
    }

    return position;
}