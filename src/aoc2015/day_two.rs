use std::str::FromStr;

use crate::utils::utils::lines_from_file;

pub fn part_one() -> i32 {
    let mut total_wrapping_paper = 0;

    let lines = lines_from_file("./src/aoc2015/data/day_two.data");
    for line in lines {
        let measurements: Vec<_> = line
            .split("x")
            .filter_map(|size| i32::from_str(size).ok())
            .collect();

        let l = *measurements.get(0).unwrap();
        let w = *measurements.get(1).unwrap();
        let h = *measurements.get(2).unwrap();
        
        let side1 = l * w;
        let side2 = w * h;
        let side3 = h * l;

        let mut smallest_side = 0;

        if side1 < side2 && side1 < side3 {
            smallest_side = side1;
        }
        else if side2 < side1 && side2 < side3 {
            smallest_side = side2;
        }
        else {
            smallest_side = side3;
        }

        total_wrapping_paper += 2*side1 + 2*side2 + 2*side3 + smallest_side;
    }

    total_wrapping_paper;
}