#[allow(dead_code)]
#[allow(unused_assignments)]

mod aoc2015;
mod utils;

fn main() {
    crate::utils::utils::print_result(aoc2015::day_two::part_one());
}
