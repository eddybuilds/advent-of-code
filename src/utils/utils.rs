use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

pub fn lines_from_file(filename: impl AsRef<Path>) -> Vec<String> {
    let file = File::open(filename).expect("no such file");
    let buf = BufReader::new(file);
    buf.lines()
        .map(|l| l.expect("Could not parse line"))
        .collect()
}

pub fn characters_from_line(line: String) -> Vec<char> {
    return line.chars().collect();
}

pub fn print_result(v: i32) {
    println!("{}", v);
}